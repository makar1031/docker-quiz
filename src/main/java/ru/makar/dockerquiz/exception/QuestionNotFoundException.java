package ru.makar.dockerquiz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Вопрос не найден")
public class QuestionNotFoundException extends RuntimeException {
}

package ru.makar.dockerquiz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Пользователь уже существует")
public class UserAlreadyExistsException extends RuntimeException {
}

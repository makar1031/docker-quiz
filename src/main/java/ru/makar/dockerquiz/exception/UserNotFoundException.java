package ru.makar.dockerquiz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Пользователь найден")
public class UserNotFoundException extends RuntimeException {
}

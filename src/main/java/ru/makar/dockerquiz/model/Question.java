package ru.makar.dockerquiz.model;

import lombok.Data;

import java.util.List;

@Data
public class Question {
    private String guid;
    private String title;
    private List<Answer> answers;
}

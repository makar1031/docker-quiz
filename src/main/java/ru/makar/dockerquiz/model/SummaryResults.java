package ru.makar.dockerquiz.model;

import lombok.Data;

import java.util.List;

@Data
public class SummaryResults {
    private boolean completed;
    private long score;
    private long total;
    private String message;
    private List<Result> answers;
}

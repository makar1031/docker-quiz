package ru.makar.dockerquiz.model;

import lombok.Data;

@Data
public class QuestionResponse {
    private boolean hasQuestion;
    private Question question;

    public QuestionResponse(Question question) {
        this.hasQuestion = question != null;
        this.question = question;
    }
}

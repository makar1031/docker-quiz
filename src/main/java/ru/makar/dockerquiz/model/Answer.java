package ru.makar.dockerquiz.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Answer {
    private String guid;
    private String text;

    @JsonIgnore
    private Boolean right;
}

package ru.makar.dockerquiz.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Results {
    private final List<Result> results = new ArrayList<>();
    private int lastQuestionIndex = 0;

    public void addResult(Result result) {
        results.add(result);
        lastQuestionIndex++;
    }
}

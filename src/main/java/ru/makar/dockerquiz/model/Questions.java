package ru.makar.dockerquiz.model;

import lombok.Data;

import java.util.List;

@Data
public class Questions {
    private List<Question> questions;
}

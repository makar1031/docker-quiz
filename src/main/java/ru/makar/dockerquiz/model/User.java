package ru.makar.dockerquiz.model;

import lombok.Data;

@Data
public class User {
    private final String guid;
    private final String userName;
}

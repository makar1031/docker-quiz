package ru.makar.dockerquiz.model;

import lombok.Data;

@Data
public class UserAnswer {
    private String userGuid;
    private String questionGuid;
    private String answerGuid;
}

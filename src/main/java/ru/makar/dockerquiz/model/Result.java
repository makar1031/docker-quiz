package ru.makar.dockerquiz.model;

import lombok.Data;

@Data
public class Result {
    private String questionText;
    private String userAnswerText;
    private String rightAnswerText;
    private boolean isRightAnswer;
}

package ru.makar.dockerquiz.dao.api;

import ru.makar.dockerquiz.model.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionsDao {
    List<Question> getQuestions();
    Optional<Question> find(String guid);
}

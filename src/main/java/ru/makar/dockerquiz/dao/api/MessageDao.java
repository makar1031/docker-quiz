package ru.makar.dockerquiz.dao.api;

public interface MessageDao {
    String getMessage(long right, long total);
}

package ru.makar.dockerquiz.dao.api;

import ru.makar.dockerquiz.model.Result;
import ru.makar.dockerquiz.model.Results;

public interface ResultsDao {
    Results getResults(String userGuid);
    Results addUserResult(String userGid, Result result);
    void removeResults(String userGuid);
}

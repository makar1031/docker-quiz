package ru.makar.dockerquiz.dao.api;

import ru.makar.dockerquiz.model.User;

public interface UsersDao {
    boolean exists(String guid);
    User create(String userName);
    boolean remove(String guid);
}

package ru.makar.dockerquiz.dao.impl;

import org.springframework.stereotype.Repository;
import org.yaml.snakeyaml.Yaml;
import ru.makar.dockerquiz.dao.api.QuestionsDao;
import ru.makar.dockerquiz.model.Question;
import ru.makar.dockerquiz.model.Questions;

import java.io.InputStream;
import java.util.*;

@Repository
public class FileSystemQuestionsDao implements QuestionsDao {
    private static final String FILE_NAME = "questions.yaml";

    private final List<Question> questions = new ArrayList<>();

    @Override
    public List<Question> getQuestions() {
        if (this.questions.isEmpty()) {
            Yaml yaml = new Yaml();
            InputStream stream = this.getClass().getClassLoader().getResourceAsStream(FILE_NAME);
            List<Question> questions = yaml.loadAs(stream, Questions.class).getQuestions();
            questions.forEach(q -> {
                q.setGuid(UUID.randomUUID().toString());
                q.getAnswers().forEach(a -> {
                    a.setGuid(UUID.randomUUID().toString());
                    if (a.getRight() == null) {
                        a.setRight(false);
                    }
                });
            });
            this.questions.addAll(questions);
        }
        return questions;
    }

    @Override
    public Optional<Question> find(String guid) {
        return questions.stream()
                .filter(q -> Objects.equals(q.getGuid(), guid))
                .findFirst();
    }
}

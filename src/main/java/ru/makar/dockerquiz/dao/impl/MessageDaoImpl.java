package ru.makar.dockerquiz.dao.impl;

import org.springframework.stereotype.Repository;
import ru.makar.dockerquiz.dao.api.MessageDao;

@Repository
public class MessageDaoImpl implements MessageDao {
    @Override
    public String getMessage(long right, long total) {
        double percent = ((double) right / (double) total) * 100;
        if (percent == 100) {
            return "Поздравляю, теперь ты Повелитель Docker 😎";
        } else if (percent > 90) {
            return "Почти идеально, я поражен! 😍";
        } else if (percent > 75) {
            return "Достойный ученик 🎓";
        } else if (percent > 50) {
            return "Одна половина позади, как на счёт второй? 😉";
        } else if (percent > 30) {
            return "Азы освоены! Я думаю, тебе следует достичь большего 💪";
        } else {
            return "Неплохое начало, но стоит ещё немного постараться 📘";
        }
    }
}

package ru.makar.dockerquiz.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.ApplicationScope;
import ru.makar.dockerquiz.dao.api.UsersDao;
import ru.makar.dockerquiz.exception.InvalidUserNameException;
import ru.makar.dockerquiz.exception.UserAlreadyExistsException;
import ru.makar.dockerquiz.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Repository
@ApplicationScope
public class InMemoryUsersDao implements UsersDao {
    private final Map<String, User> usersStore = new HashMap<>();

    @Override
    public boolean exists(String guid) {
        return usersStore.containsKey(guid);
    }

    @Override
    public User create(String userName) {
        if (userName == null || userName.isEmpty()) {
            throw new InvalidUserNameException();
        }
        boolean exists = usersStore.values().stream()
            .map(User::getUserName)
            .anyMatch(n -> Objects.equals(n, userName));
        if (exists) {
            log.error("User already exists: {}", userName);
            throw new UserAlreadyExistsException();
        }
        User user = new User(UUID.randomUUID().toString(), userName);
        usersStore.put(user.getGuid(), user);
        log.info("User created: {}", user);
        return user;
    }

    @Override
    public boolean remove(String guid) {
        User removedUser = usersStore.remove(guid);
        if (removedUser != null) {
            log.info("User removed: {}", removedUser);
            return true;
        } else {
            log.error("Unable remove user, user with guid '{}' not exists", guid);
            return false;
        }
    }
}

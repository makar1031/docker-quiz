package ru.makar.dockerquiz.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.ApplicationScope;
import ru.makar.dockerquiz.dao.api.ResultsDao;
import ru.makar.dockerquiz.model.Result;
import ru.makar.dockerquiz.model.Results;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Repository
@ApplicationScope
public class InMemoryResultsDao implements ResultsDao {
    private final Map<String, Results> userResults = new HashMap<>();

    @Override
    public Results getResults(String userGuid) {
        return this.userResults.computeIfAbsent(userGuid, (guid) -> new Results());
    }

    @Override
    public Results addUserResult(String userGid, Result result) {
        Results userResults = getResults(userGid);
        userResults.addResult(result);
        return userResults;
    }

    @Override
    public void removeResults(String userGuid) {
        log.info("Remove results for user: {}", userGuid);
        userResults.remove(userGuid);
    }
}

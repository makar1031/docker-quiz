package ru.makar.dockerquiz.service.api;

import ru.makar.dockerquiz.model.User;

public interface UsersService {
    User create(String userName);
    boolean remove(String guid);
}

package ru.makar.dockerquiz.service.api;

import ru.makar.dockerquiz.model.Question;
import ru.makar.dockerquiz.model.UserAnswer;

import java.util.Optional;

public interface QuestionsService {
    Optional<Question> getCurrent(String userGuid);
    Optional<Question> sendAnswer(UserAnswer answer);
}

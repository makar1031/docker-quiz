package ru.makar.dockerquiz.service.api;

import ru.makar.dockerquiz.model.SummaryResults;

public interface ResultsService {
    SummaryResults getResults(String userGuid);
}

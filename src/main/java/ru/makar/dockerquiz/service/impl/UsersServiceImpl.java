package ru.makar.dockerquiz.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;
import ru.makar.dockerquiz.dao.api.ResultsDao;
import ru.makar.dockerquiz.dao.api.UsersDao;
import ru.makar.dockerquiz.model.User;
import ru.makar.dockerquiz.service.api.UsersService;

import java.io.Serializable;

@Service
@ApplicationScope
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService, Serializable {
    private final UsersDao usersDao;
    private final ResultsDao resultsDao;

    @Override
    public User create(String userName) {
        return usersDao.create(userName);
    }

    @Override
    public boolean remove(String guid) {
        boolean removed = usersDao.remove(guid);
        resultsDao.removeResults(guid);
        return removed;
    }
}

package ru.makar.dockerquiz.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;
import ru.makar.dockerquiz.dao.api.QuestionsDao;
import ru.makar.dockerquiz.dao.api.ResultsDao;
import ru.makar.dockerquiz.dao.api.UsersDao;
import ru.makar.dockerquiz.exception.AnswerNotFoundException;
import ru.makar.dockerquiz.exception.QuestionNotFoundException;
import ru.makar.dockerquiz.exception.UserNotFoundException;
import ru.makar.dockerquiz.model.*;
import ru.makar.dockerquiz.service.api.QuestionsService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@ApplicationScope
@RequiredArgsConstructor
public class QuestionsServiceImpl implements QuestionsService {
    private final QuestionsDao questionsDao;
    private final UsersDao usersDao;
    private final ResultsDao resultsDao;

    @Override
    public Optional<Question> getCurrent(String userGuid) {
        if (!usersDao.exists(userGuid)) {
            throw new UserNotFoundException();
        }

        return getQuestion(resultsDao.getResults(userGuid));
    }

    @Override
    public Optional<Question> sendAnswer(UserAnswer answer) {
        String userGuid = answer.getUserGuid();
        if (!usersDao.exists(userGuid)) {
            throw new UserNotFoundException();
        }

        Result result = getAnswerResult(answer);
        return getQuestion(resultsDao.addUserResult(userGuid, result));
    }

    private Optional<Question> getQuestion(Results userResults) {
        int index = userResults.getLastQuestionIndex();
        List<Question> questions = questionsDao.getQuestions();
        return Optional.ofNullable(index < questions.size() ? questions.get(index) : null);
    }

    private Result getAnswerResult(UserAnswer answer) {
        Question question = questionsDao.find(answer.getQuestionGuid()).orElseThrow(QuestionNotFoundException::new);

        Answer selectedAnswer = question.getAnswers().stream()
                .filter(a -> Objects.equals(a.getGuid(), answer.getAnswerGuid()))
                .findFirst()
                .orElseThrow(AnswerNotFoundException::new);
        Answer rightAnswer = question.getAnswers().stream()
            .filter(Answer::getRight)
            .findFirst()
            .orElseThrow(AnswerNotFoundException::new);

        Result result = new Result();
        result.setQuestionText(question.getTitle());
        result.setUserAnswerText(selectedAnswer.getText());
        result.setRightAnswerText(rightAnswer.getText());
        result.setRightAnswer(selectedAnswer.getRight());
        log.debug("User {} answer result: {}", answer.getUserGuid(), result);
        return result;
    }
}

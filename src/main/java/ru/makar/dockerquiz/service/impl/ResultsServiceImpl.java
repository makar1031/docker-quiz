package ru.makar.dockerquiz.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.makar.dockerquiz.dao.api.MessageDao;
import ru.makar.dockerquiz.dao.api.QuestionsDao;
import ru.makar.dockerquiz.dao.api.ResultsDao;
import ru.makar.dockerquiz.model.Result;
import ru.makar.dockerquiz.model.Results;
import ru.makar.dockerquiz.model.SummaryResults;
import ru.makar.dockerquiz.service.api.ResultsService;

@Service
@RequiredArgsConstructor
public class ResultsServiceImpl implements ResultsService {
    private final ResultsDao resultsDao;
    private final QuestionsDao questionsDao;
    private final MessageDao messageDao;

    @Override
    public SummaryResults getResults(String userGuid) {
        Results userResults = resultsDao.getResults(userGuid);
        long right = userResults.getResults().stream().filter(Result::isRightAnswer).count();
        long completed = userResults.getResults().size();
        long total = questionsDao.getQuestions().size();

        SummaryResults results = new SummaryResults();
        results.setCompleted(completed == total);
        results.setScore(right);
        results.setTotal(total);

        if (results.isCompleted()) {
            results.setMessage(messageDao.getMessage(right, total));
            results.setAnswers(userResults.getResults());
        }

        return results;
    }
}

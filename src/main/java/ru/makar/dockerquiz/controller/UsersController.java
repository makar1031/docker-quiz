package ru.makar.dockerquiz.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.makar.dockerquiz.model.User;
import ru.makar.dockerquiz.service.api.UsersService;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {
    private final UsersService usersService;

    @PostMapping
    public User create(@RequestBody String username) {
        return usersService.create(username);
    }

    @DeleteMapping
    public ResponseEntity<Void> remove(@RequestParam("guid") String guid) {
        return ResponseEntity.status(usersService.remove(guid) ? HttpStatus.OK : HttpStatus.NOT_FOUND).build();
    }
}

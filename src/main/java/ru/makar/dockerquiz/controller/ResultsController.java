package ru.makar.dockerquiz.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.makar.dockerquiz.model.SummaryResults;
import ru.makar.dockerquiz.service.api.ResultsService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/results")
public class ResultsController {
    private final ResultsService service;

    @GetMapping
    public SummaryResults getResults(@RequestParam("userGuid") String userGuid) {
        return service.getResults(userGuid);
    }
}

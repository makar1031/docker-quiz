package ru.makar.dockerquiz.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.makar.dockerquiz.model.QuestionResponse;
import ru.makar.dockerquiz.model.UserAnswer;
import ru.makar.dockerquiz.service.api.QuestionsService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/questions")
public class QuestionsController {
    private final QuestionsService service;

    @GetMapping
    public QuestionResponse getCurrent(@RequestParam("userGuid") String userGuid) {
        return new QuestionResponse(service.getCurrent(userGuid).orElse(null));
    }

    @PostMapping
    public QuestionResponse sendAnswer(@RequestBody UserAnswer answer) {
        return new QuestionResponse(service.sendAnswer(answer).orElse(null));
    }
}

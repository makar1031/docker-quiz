FROM openjdk:8-jre-alpine
WORKDIR /app
COPY target/docker-quiz.jar ./app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]
